#ifndef UART_H
#define UART_H
#include <stdint.h>
#include "app_uart.h"

void uart_error_handle(app_uart_evt_t *p_event);
void uart_init(uint32_t err_code);
void uartWriteByteArray(const uint8_t *byteArray, uint32_t byteArrayLen);

#endif // UART_H
