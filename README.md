# Procesamiento de señales (fundamentos)

## Trabajo final

Captura de aceleración y cálculo en MCU de:

- Máximo
- Mínimo
- Valor RMS
- FFT

## Demo

![](visualize_demo.mov)

### Prerequisitos

Para compilar este repositorio es necesario tener:

- Docker

Para grabar el MCU (_flash_) es necesario tener:

- nRF Command Line Tools
